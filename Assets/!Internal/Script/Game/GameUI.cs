using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Internal.Script.Game
{
    public class GameUI : MonoBehaviour
    {
        [SerializeField] private TMP_Text _coinsText;
        [SerializeField] private Button _startButton;
        
        private void Update()
        {
            _coinsText.text = GameHandler.Instance.coins.ToString();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _startButton.onClick.Invoke();
            }
        }
    }
}