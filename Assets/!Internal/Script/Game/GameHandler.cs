using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Internal.Script.Game
{
    public class GameHandler : MonoBehaviour
    {
        private static GameHandler _instance;
        public static GameHandler Instance => _instance;
        
        private int _coins;
        public int coins => _coins;

        private GameStatus _gameStatus;
        public GameStatus gameStatus => _gameStatus;
            
        public enum GameStatus
        {
            NotStarted,
            Playing,
            Over
        } 
            
            
        private void Awake()
        {
            if (!_instance)
                _instance = this;
            else 
                Destroy(gameObject);
        }


        private void Start()
        {
            Application.targetFrameRate = 30;
        }


        public void AddCoin()
        {
            _coins++;
        }


        public void StartGame()
        {
            _gameStatus = GameStatus.Playing;
        }


        public void GameOver()
        {
            _gameStatus = GameStatus.Over;
            RestartGame();
        }
        

        public void RestartGame()
        {
            _gameStatus = GameStatus.NotStarted;
            SceneManager.LoadScene(0);
        }
    }
}