using UnityEngine;

namespace _Internal.Script.Utilities
{
    public class CheckCameraView
    {
        public static bool IsObjectInCameraView(Camera camera, Transform target, bool addScale = true)
        {
            Vector3 targetPos = addScale
                ? new Vector2(target.position.x + target.localScale.x/2, target.position.y)
                : target.position; 
            var objectViewPos = camera.WorldToViewportPoint(targetPos);
            return objectViewPos.x >= 0
                   && objectViewPos.x <= 1
                   && objectViewPos.y >= 0
                   && objectViewPos.y <= 1;

        }
    }
}