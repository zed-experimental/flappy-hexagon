using _Internal.Script.Utilities;
using UnityEngine;

namespace _Internal.Script.Env
{
    public class CeilFloor : MonoBehaviour
    {
        private bool _seen = false;

        private void Update()
        {
            if (!CheckCameraView.IsObjectInCameraView(Camera.main, transform) && _seen)
                Destroy(gameObject);
            else if (CheckCameraView.IsObjectInCameraView(Camera.main, transform))
                _seen = true;
        }

    }
}