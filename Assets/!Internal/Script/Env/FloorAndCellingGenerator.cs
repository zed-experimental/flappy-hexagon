using _Internal.Script.Game;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;

namespace _Internal.Script.Env
{
    public class FloorAndCellingGenerator : MonoBehaviour
    {
        [SerializeField] private CeilFloor _cellingAndFloorPrefab;
        [SerializeField] private Transform _ceilingParent;
        [SerializeField] private Transform _floorParent;
        [SerializeField] private float _timeToSpawn;
        
        
        private GameObject _currentFloor;
        private GameObject _currentCeiling;
        private float _timeCounter;
        

        private void Start()
        {
            _currentFloor = _floorParent.gameObject;
            _currentCeiling = _ceilingParent.gameObject;
            SpawnNewCeilingAndFloor();
        }

        private void Update()
        {
            if (GameHandler.Instance.gameStatus == GameHandler.GameStatus.Playing)
            {
                if (_timeCounter <= 0)
                {
                    SpawnNewCeilingAndFloor();
                    _timeCounter = _timeToSpawn;
                }
                else
                {
                    _timeCounter -= Time.deltaTime;
                }
            }
        }


        private void SpawnNewCeilingAndFloor()
        {
            Vector2 floorSpawnPos = new Vector2(_currentFloor.transform.position.x + _currentFloor.transform.localScale.x, _floorParent.position.y); 
            Vector2 ceilingSpawnPos = new Vector2(_currentCeiling.transform.position.x + _currentCeiling.transform.localScale.x, _ceilingParent.position.y); 
            
            _currentFloor = Instantiate(_cellingAndFloorPrefab, floorSpawnPos, Quaternion.identity, _floorParent).gameObject;
            _currentCeiling = Instantiate(_cellingAndFloorPrefab, ceilingSpawnPos, Quaternion.identity, _ceilingParent).gameObject;
        }
    }
}