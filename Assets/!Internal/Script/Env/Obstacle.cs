using System.Collections;
using _Internal.Script.Utilities;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Internal.Script.Env
{
    public class Obstacle : MonoBehaviour
    {
        [SerializeField] private float _floatingTime;
        [SerializeField] private float _floatingDistance;

        private bool _seen = false;

        private void Start()
        {
            StartCoroutine(StartFloat(Random.Range(0, 2) == 1 ? Vector2.up : Vector2.down));
        }


        private void Update()
        {
            if (!CheckCameraView.IsObjectInCameraView(Camera.main, transform) && _seen)
                Destroy(gameObject);
            else if (CheckCameraView.IsObjectInCameraView(Camera.main, transform))
                _seen = true;
        }
        

        private IEnumerator StartFloat(Vector2 dir)
        {
            Vector2 targetPos = (Vector2)transform.position + (dir * _floatingDistance);  
            
            float timeCounter = 0;
            while (timeCounter < _floatingTime)
            {
                transform.position = Vector2.Lerp(transform.position, targetPos, timeCounter/_floatingTime * Time.deltaTime);
                timeCounter += Time.deltaTime;
                yield return null;
            }

            StartCoroutine(StartFloat(dir * -1));
        }
    }
}