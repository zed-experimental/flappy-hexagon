using _Internal.Script.Game;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace _Internal.Script.Env
{
    public class ObstaclesGenerator : MonoBehaviour
    {
        [SerializeField] private Obstacle _obstacle;
        [SerializeField] private int _obstaclesPerSpawn;
        [SerializeField] private float _distanceBtwObstacles;
        [SerializeField] private Vector2 _obstaclesYMinMax;
        [SerializeField] private float _obstaclesSpawnDuration;
        [SerializeField] private Transform _obstaclesStartPos;

        private Obstacle _currentObstacle;
        private float _timeCounter;
        

        private void Update()
        {
            if (GameHandler.Instance.gameStatus == GameHandler.GameStatus.Playing)
            {
                if (_timeCounter <= 0)
                {
                    _timeCounter = _obstaclesSpawnDuration;
                    SpawnObstacle();
                }
                else
                {
                    _timeCounter -= Time.deltaTime;
                }
            }
        }

        private void SpawnObstacle()
        {
            var pos = new Vector2( 
                _currentObstacle == null ? 
                    _obstaclesStartPos.position.x : 
                    _currentObstacle.transform.position.x 
                    + _distanceBtwObstacles,
                0);
            
            for (int i = 0; i < _obstaclesPerSpawn; i++)
            {
                pos.y = Random.Range(_obstaclesYMinMax.x, _obstaclesYMinMax.y);
                _currentObstacle = Instantiate(_obstacle, pos, Quaternion.identity, _obstaclesStartPos);
                _currentObstacle.transform.Rotate(Vector3.forward, Random.Range(0, 360));
            }
        }
    }
}