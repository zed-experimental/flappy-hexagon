using _Internal.Script.Game;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Internal.Script.Env
{
    public class CoinsGenerator : MonoBehaviour
    {
        [SerializeField] private Coin _coin;
        [SerializeField] private float _distanceBtwCoins;
        [SerializeField] private Vector2 _coinsYMinMax;
        [SerializeField] private float _coinsSpawnDuration;
        [SerializeField] private Transform _coinsParent;

        private Vector2 _currentCoinPos;
        private float _timeCounter;


        private void Start()
        {
            _currentCoinPos = _coinsParent.position;
        }

        
        private void Update()
        {
            if (GameHandler.Instance.gameStatus == GameHandler.GameStatus.Playing)
            {
                if (_timeCounter <= 0)
                {
                    _timeCounter = _coinsSpawnDuration;
                    SpawnCoin();
                }
                else
                {
                    _timeCounter -= Time.deltaTime;
                }
            }
        }

        private void SpawnCoin()
        {
            var pos = new Vector2(_currentCoinPos.x + _distanceBtwCoins,
                Random.Range(_coinsYMinMax.x, _coinsYMinMax.y));

            _currentCoinPos = Instantiate(_coin, pos, Quaternion.identity, _coinsParent).transform.position;
        }
    }
}