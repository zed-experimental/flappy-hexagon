using _Internal.Script.Game;
using _Internal.Script.Utilities;
using UnityEngine;

namespace _Internal.Script.Env
{
    public class Coin : MonoBehaviour
    {
        [SerializeField] private float _moveToTargetSpeed;
        
        private Transform _target;
        private bool _seen;

        private void Update()
        {
            if (_target)
            {
                transform.position = Vector3.MoveTowards( transform.position,_target.position, (_moveToTargetSpeed * Time.deltaTime));
            }
            if (!CheckCameraView.IsObjectInCameraView(Camera.main, transform) && _seen)
                Destroy(gameObject);
            else if (CheckCameraView.IsObjectInCameraView(Camera.main, transform))
                _seen = true;
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                _target = other.transform;
            }
        }


        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                _target = null;
            }
        }
        

        private void OnCollisionEnter2D(Collision2D other)
        {
            GameHandler.Instance.AddCoin();
            Destroy(gameObject);
        }
    }
}