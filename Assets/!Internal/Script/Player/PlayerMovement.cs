using _Internal.Script.Game;
using UnityEngine;

namespace _Internal.Script.Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float _movementSpeed;
        [SerializeField] private float _jumpForce;
        [SerializeField] private float _jumpDuration;

        private Rigidbody2D _rigidbody2D;
        private Rigidbody2D rigidbody2D
        {
            get
            {
                if (!_rigidbody2D)
                    _rigidbody2D = GetComponent<Rigidbody2D>();

                return _rigidbody2D;
            }
        }

        private Vector3 _jumpVector;
        

        private void Update()
        {
            if (GameHandler.Instance.gameStatus == GameHandler.GameStatus.Playing)
            {
                if (rigidbody2D.IsSleeping())
                    rigidbody2D.WakeUp();

                if (Input.GetKey(KeyCode.Space))
                    _jumpVector = Vector3.up;
                else
                    _jumpVector = Vector3.down;
            }
        }

        private void FixedUpdate()
        {
            if (GameHandler.Instance.gameStatus == GameHandler.GameStatus.Playing)
            {
                rigidbody2D.velocity = ((Vector3.right * _movementSpeed) + (_jumpVector * _jumpForce));
            }
        }


        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Obstacle"))
            {
                GameHandler.Instance.GameOver();
                rigidbody2D.constraints = RigidbodyConstraints2D.FreezePosition;
            }
        }
    }
}