using UnityEngine;

namespace _Internal.Script.Player
{
    public class Trail : MonoBehaviour
    {
        public void OnAnimationEnd()
        {
            Destroy(gameObject);
        }
    }
}