using UnityEngine;

namespace _Internal.Script.Player
{
    public class PlayerTrail : MonoBehaviour
    {
        [SerializeField] private GameObject _trailObject;
        [SerializeField] private float _timeForSpawn;


        private float _timeCounter;
        
        
        private void Update()
        {
            if (_timeCounter <= 0)
            {
                var gm = Instantiate(_trailObject, transform.position, Quaternion.identity);
                _timeCounter = _timeForSpawn;
            }
            else
            {
                _timeCounter -= Time.deltaTime;
            }
        }
    }
}