using UnityEngine;

namespace _Internal.Script.Player
{
    public class CameraPlayerFollow : MonoBehaviour
    {
        [SerializeField] private Transform _player;
        [SerializeField] private Vector3 _offset;

        private void Update()
        {
            transform.position = new Vector3(_player.position.x, transform.position.y, transform.position.z) + _offset;
        }
    }
}